from django.db import models


class Book(models.Model):
    book_title = models.CharField(max_length=200)
    kind_of_book = models.CharField(max_length=70)
    author_second_name = models.CharField(max_length=100)
    author_first_name = models.CharField(max_length=100,default='')
    author_full_name = models.CharField(max_length=200,default='')
    published_date = models.DateField()
    number_of_pages = models.IntegerField(blank=True)
    price = models.IntegerField(blank=True)
    io_stock = models.BooleanField()

    def __str__(self):
        return self.book_title
