from django.core.management.base import BaseCommand
from django.utils import timezone

from book_test.models import Book


class Command(BaseCommand):
    help = 'We can get all name of book in our database.'

    def add_arguments(self, parser):
        parser.add_argument('-a', '--all', type=str,
                            help="We know the price of book.")
        parser.add_argument('-pr', '--price', type=int,
                            help="We know the price of book.")

    def handle(self, *args, **options):
        all = options['all']
        price = options['price']
        books = Book.objects.all()
        if all:
            for book in books:
                self.stdout.write(f"We have: {book.id} - {book}")
        elif price:
            book = Book.objects.get(id=price)
            self.stdout.write(f"The price of {book} - {book.price}")
